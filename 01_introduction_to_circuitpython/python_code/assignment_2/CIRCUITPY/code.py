# DIY Electronics 02
# Assignment 2: RGB LED
# Example Solution
# 2022 Marian Weger


# REQUIREMENTS
# (put in libs folder)
# - neopixel: neopixel.py from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - colorsys: colorsys.mpy from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/


# LOAD LIBRARIES
import board # for the microcontroller itself
import neopixel # for on-board RGB LED
import colorsys # color conversions (HSV to RGB)
import math # needed for math in the hsv2rgb function


# SETUP
rgb = neopixel.NeoPixel(board.NEOPIXEL, 1) # set up the RGB LED
rgb.brightness = 0.02 # set brightness to a low value
# (Note: brightness simply scales the 8bit RGB values. Low brightness leads to low color resolution.)

# It seems that it is not enough to simply set it once like this:
# rgb[0] = (255,127,0)
# color needs to be constantly updated in the main loop


# PARAMETERS
# Define HSV color
hue = 0.25 # hue (angle between 0 and 360)
sat = 1.0 # saturation (between 0 and 1)
val = 1.0 # value (between 0 and 1. set to 1, as brightness is anyway set directly)


# ALTERNATIVE TO COLORSYS
# https://code.activestate.com/recipes/576919-python-rgb-and-hsv-conversion/
# function for conversion from HSV to RGB
# HSV values are float between 0 and 1, except hue going from 0 to 360 (angle). RGB values are 8bit integer from 0 to 255.
def hsv2rgb(h, s, v):
    h = float(h)
    s = float(s)
    v = float(v)
    h60 = h / 60.0
    h60f = math.floor(h60)
    hi = int(h60f) % 6
    f = h60 - h60f
    p = v * (1 - s)
    q = v * (1 - f * s)
    t = v * (1 - (1 - f) * s)
    r, g, b = 0, 0, 0
    if hi == 0: r, g, b = v, t, p
    elif hi == 1: r, g, b = q, v, p
    elif hi == 2: r, g, b = p, v, t
    elif hi == 3: r, g, b = p, q, v
    elif hi == 4: r, g, b = t, p, v
    elif hi == 5: r, g, b = v, p, q
    r, g, b = int(r * 255), int(g * 255), int(b * 255)
    return r, g, b


# PRE-COMPUTATIONS

# convert HSV to RGB (via colorsys library)
rgbColor = colorsys.hsv_to_rgb(hue, sat, val)

# convert HSV to RGB (via function definition)
#rgbColor = hsv2rgb(hue*360, sat, val) # note: hue needs to by multiplied by 360 here


# MAIN LOOP
while True:

    rgb[0] = rgbColor # set LED to RGB color

