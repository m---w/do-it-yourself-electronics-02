# DIY Electronics 02
# Assignment 4: time and multitasking
# Example Solution with multithreading
# 2022 Marian Weger


# REQUIREMENTS
# (put in libs folder)
# - neopixel: neopixel.py from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - colorsys: colorsys.mpy from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - asyncio: asyncio folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - asyncio needs adafruit_ticks: adafruit_ticks.mpy from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/



# LIBRARIES
import board # for the microcontroller itself
import math # needed for additional math functions
import digitalio # for digital ins/outs
import analogio # for analog ins/outs
import time # for time measurment
import neopixel # for on-board RGB LED
import colorsys # color conversions (HSV to RGB)
import asyncio # for asynchronous task scheduling / multithreading


# SETUP

light = analogio.AnalogIn(board.A5) # set up analog input A5 (light sensor)

led = digitalio.DigitalInOut(board.D13) # setup digital i/o pin 13 (onboard led)
led.direction = digitalio.Direction.OUTPUT # set pin as output

rgb = neopixel.NeoPixel(board.NEOPIXEL, 1) # set up the RGB LED
rgb.brightness = 0.02 # set brightness to a low value


# PARAMETERS
# setup debug printing
printPeriod = 0.05 # printing period
ledPeriod = 1.5 # blinking period
ledDur = 0.1 # active duration
ledSleep = ledPeriod - ledDur # inactive duration


# FUNCTIONS

# clip
# clips value between outMin and outMax
def clip(inVal, outMin, outMax):
    return min(max(inVal, outMin), outMax) #  clip at minimum and maximum value
    # note: min() takes minimum (the smallest) of its arguments, hence sets the maximum output value
    # note: max() takes the maximum (the largest) of its arguments, hence sets the minimum output value

# prune
# clips either below outMin, above outMax, or both, depending on type
def prune(inVal, outMin, outMax, clipType='minmax'):
    if clipType == 'minmax':
        outVal = clip(inVal, outMin, outMax) # clip at minimum and maxumum value
    elif clipType == 'min':
        outVal = max(inVal,outMin) # clip at minimum value
    elif clipType == 'max':
        outVal = min(inVal,outMax) # clip at maximum value
    elif 'none':
        outVal = inVal
    else:
        print('ERROR in prune: incorrect clipType!')
    return outVal


# linlin
# map linear to linear range
def linlin(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    outVal = ( (prune(inVal, inMin, inMax, clipType) - inMin) / (inMax - inMin) ) * (outMax - outMin) + outMin
    return float(outVal) # make sure the output is always float

# linlin01
# map linear to linear range between 0 and 1, including minmax clipping
# faster alternative to linlin(inVal, inMin, inMax, 0, 1, 'minmax')
def linlin01(inVal, inMin, inMax):
    outVal = min(max( (inVal - inMin) / (inMax - inMin), 0), 1)
    return float(outVal)

# linexp
# map linear to exponential range
def linexp(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    if outMin==0 or outMax==0:
        print('ERROR in linexp: ZERO not possible for exponential range!')
    else:
        outVal = math.pow(outMax/outMin, (inVal-inMin)/(inMax-inMin)) * outMin
        return float( prune(outVal, outMin, outMax, clipType) )

# explin
# map exponential to linear range
def explin(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    outVal = ( (math.log(prune(inVal, inMin, inMax, clipType)/inMin)) / (math.log(inMax/inMin)) ) * (outMax-outMin) + outMin
    return float(outVal)



# SCHEDULER TASKS

# Blinking Task
async def blinkTask():
    while True:
        led.value = True # set digital output to High
        await asyncio.sleep(ledDur) # wait for 0.1s
        led.value = False # set digital output to Low
        await asyncio.sleep(ledSleep) # wait for 1.4s

# Printing Task
async def printTask():
    while True:
        lightLinexp = linexp(light.value, 900, 60000, 0.01, 1) # map light value
        print(f'light (linexp): {lightLinexp}') # print mapped value
        await asyncio.sleep(printPeriod) # wait for 50ms

# RGB Task
async def rgbTask():
    while True:
        # Change RGB color depending on light resistor
        hue = linlin01(light.value, 900, 60000) # map light sensor to hue
        rgbColor = colorsys.hsv_to_rgb(hue, 1.0, 1.0) # convert HSV to RGB
        rgb[0] = rgbColor # set LED to RGB color
        await asyncio.sleep(0.033) # update color every 33ms. 30 frames per second (FPS) is enough.


# MAIN TASK
async def main():
    # Schedule all calls *concurrently*:
    await asyncio.gather(
        # put tasks in here
        blinkTask(),
        printTask(),
        rgbTask()
    )


# RUN SCHEDULER
asyncio.run(main())
