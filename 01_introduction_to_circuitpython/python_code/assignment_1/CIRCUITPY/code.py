# DIY Electronics 02
# Assignment 1: blinky blinky
# Example Solution
# 2022 Marian Weger

# LOAD LIBRARIES
import board # for the microcontroller itself
import digitalio # for digital inputs/outputs
import time # for measuring time

# SETUP
led = digitalio.DigitalInOut(board.D13) # setup digital i/o pin 13
led.direction = digitalio.Direction.OUTPUT # set pin as output

# PARAMETERS
# (given in the instructions)
ledPeriod = 1.5 # blinking period
ledDur = 0.1 # active duration

# PRE-COMPUTATIONS
ledSleep = ledPeriod - ledDur # inactive duration

# MAIN LOOP
while True:
    led.value = True # set digital output to High
    time.sleep(ledDur) # wait for 0.1s
    led.value = False # set digital output to Low
    time.sleep(ledSleep) # wait for 1.4s