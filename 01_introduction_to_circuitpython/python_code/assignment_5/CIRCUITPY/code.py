# DIY Electronics 02
# Assignment 5: Rotary Encoder and USB-MIDI
# Example Solution
# 2022 Marian Weger


# REQUIREMENTS
# (put in libs folder)
# - neopixel: neopixel.py from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - colorsys: colorsys.mpy from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - adafruit_midi: adafruit_midi folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/


# LIBRARIES
import board # for the microcontroller itself
import math # needed for additional math functions
import digitalio # for digital ins/outs
import analogio # for analog ins/outs
import time # for time measurment
import neopixel # for on-board RGB LED
import colorsys # color conversions (HSV to RGB)
import usb_midi # USB MIDI device
import adafruit_midi # MIDI protocol encoder/decoder library
from adafruit_midi.control_change import ControlChange # shortcut to control_change


# SETUP MIDI
usb_midi_in_channel = 1  # pick your USB MIDI in channel here, 1-16
usb_midi_out_channel = 1  # pick your USB MIDI out channel here, 1-16
midi = adafruit_midi.MIDI(
    midi_out = usb_midi.ports[1],
    out_channel = 0, #
    midi_in = usb_midi.ports[0],
    # in_channel = 0, # listen to all channels
    in_buf_size = 100 # default is 30 bytes. Let's set it a bit higher.
)


# SETUP ROTARY ENCODER
rotCLK = digitalio.DigitalInOut(board.D10)
rotCLK.direction = digitalio.Direction.INPUT
rotDT = digitalio.DigitalInOut(board.D11)
rotDT.direction = digitalio.Direction.INPUT
rotSW = digitalio.DigitalInOut(board.D12)
rotSW.direction = digitalio.Direction.INPUT
rotSW.pull = digitalio.Pull.UP

# rotary encoder init
rotary = 0
clkLastState = rotCLK.value
swLastState = rotSW.value
clkLastTime = -1

# SETUP light sensor
light = analogio.AnalogIn(board.A5) # set up analog input A5 (light sensor)
lightLastState = 0
lastLightTime = -1
lightPeriod = 0.05

# SETUP LED13
led = digitalio.DigitalInOut(board.D13) # setup digital i/o pin 13 (onboard led)
led.direction = digitalio.Direction.OUTPUT # set pin as output

# SETUP neopixel
rgb = neopixel.NeoPixel(board.NEOPIXEL, 1) # set up the RGB LED
rgb.brightness = 1.0 # set brightness
hue = 0.0
sat = 0.0
val = 0.0


# FUNCTIONS

# clip
# clips value between outMin and outMax
def clip(inVal, outMin, outMax):
    return min(max(inVal, outMin), outMax) #  clip at minimum and maximum value
    # note: min() takes minimum (the smallest) of its arguments, hence sets the maximum output value
    # note: max() takes the maximum (the largest) of its arguments, hence sets the minimum output value

# prune
# clips either below outMin, above outMax, or both, depending on type
def prune(inVal, outMin, outMax, clipType='minmax'):
    if clipType == 'minmax':
        outVal = clip(inVal, outMin, outMax) # clip at minimum and maxumum value
    elif clipType == 'min':
        outVal = max(inVal,outMin) # clip at minimum value
    elif clipType == 'max':
        outVal = min(inVal,outMax) # clip at maximum value
    elif 'none':
        outVal = inVal
    else:
        print('ERROR in prune: incorrect clipType!')
    return outVal

# linlin
# map linear to linear range
def linlin(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    outVal = ( (prune(inVal, inMin, inMax, clipType) - inMin) / (inMax - inMin) ) * (outMax - outMin) + outMin
    return float(outVal) # make sure the output is always float

# linlin01
# map linear to linear range between 0 and 1, including minmax clipping
# faster alternative to linlin(inVal, inMin, inMax, 0, 1, 'minmax')
def linlin01(inVal, inMin, inMax):
    outVal = min(max( (inVal - inMin) / (inMax - inMin), 0), 1)
    return float(outVal)

# linexp
# map linear to exponential range
def linexp(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    if outMin==0 or outMax==0:
        print('ERROR in linexp: ZERO not possible for exponential range!')
    else:
        outVal = math.pow(outMax/outMin, (inVal-inMin)/(inMax-inMin)) * outMin
        return float( prune(outVal, outMin, outMax, clipType) )

# explin
# map exponential to linear range
def explin(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    outVal = ( (math.log(prune(inVal, inMin, inMax, clipType)/inMin)) / (math.log(inMax/inMin)) ) * (outMax-outMin) + outMin
    return float(outVal)

# rgbUpdate
# shortcut for updating the neopixel
def rgbUpdate():
    rgb[0] = colorsys.hsv_to_rgb(hue, sat, val)


# MAIN LOOP
while True:

    # Store the current time to refer to later.
    now = time.monotonic() # what time is it now?

    # Rotary Encoder
    clkState = rotCLK.value
    dtState = rotDT.value
    if clkState != clkLastState:

        # change increment based on rotation speed
        clkPeriod = now-clkLastTime # measure time since last change
        increment = explin(clkPeriod, 0.005, 0.25, 4, 1) # set increment per tick depending on time period between ticks
        clkLastTime = now

        # increment or decrement value depending on direction
        if dtState != clkState:
            rotary = rotary + increment
        else:
            rotary = rotary - increment

        rotary = min(max(round(rotary), 0), 127) # round and clip value
        midi.send(ControlChange(1, rotary)) # form a MIDI CC message and send it
        print(f'rotary: {rotary}') # Print status of rotary
    clkLastState = clkState

    # Push Button of Rotary Encoder
    swState = not rotSW.value
    if swState != swLastState:
        midi.send(ControlChange(2, int(swState*127)))
        print(f'button: {swState}') #  print status of push button
    swLastState = swState

    # Light Sensor
    if now >= lastLightTime + lightPeriod:
        lightState = round(linlin(light.value, 900, 60000, 0, 127)) # map light value
        if lightState != lightLastState:
            midi.send(ControlChange(3, lightState))
            # print(f'light: {lightState}')
        lightLastState = lightState
        lastLightTime = now

    # MIDI DISPATCHER
    midiRawIn = midi.receive()
    if midiRawIn is not None:
        # CC
        if isinstance(midiRawIn, ControlChange):
            midiCH = midiRawIn.channel
            midiCC = midiRawIn.control
            midiVal = midiRawIn.value
            if midiCH == 0:
                if midiCC == 1:
                    rotary = midiVal
                    print(f'rotary: {rotary}')
                if midiCC == 4:
                    sat = linlin(midiVal, 0, 127, 0.0, 1.0)
                    rgbUpdate()
                    print(f'saturation: {sat}')
                if midiCC == 5:
                    val = linlin(midiVal, 0, 127, 0.0, 1.0)
                    rgbUpdate()
                    print(f'value: {val}')
                if midiCC == 6:
                    if midiVal>0:
                        led.value = True
                    else:
                        led.value = False
                    print(f'LED13: {led.value}')