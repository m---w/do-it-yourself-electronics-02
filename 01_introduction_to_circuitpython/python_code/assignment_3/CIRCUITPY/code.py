# DIY Electronics 02
# Assignment 3: photoresistor
# Example Solution
# 2022 Marian Weger


# LIBRARIES
import board # for the microcontroller itself
import math # needed for additional math functions
import analogio # for analog ins/outs
import time # for time measurment


# SETUP
light = analogio.AnalogIn(board.A5) # set up analog input A5


# FUNCTIONS

# clip
# clips value between outMin and outMax
def clip(inVal, outMin, outMax):
    return min(max(inVal, outMin), outMax) #  clip at minimum and maximum value
    # note: min() takes minimum (the smallest) of its arguments, hence sets the maximum output value
    # note: max() takes the maximum (the largest) of its arguments, hence sets the minimum output value

# prune
# clips either below outMin, above outMax, or both, depending on type
def prune(inVal, outMin, outMax, clipType='minmax'):
    if clipType == 'minmax':
        outVal = clip(inVal, outMin, outMax) # clip at minimum and maxumum value
    elif clipType == 'min':
        outVal = max(inVal,outMin) # clip at minimum value
    elif clipType == 'max':
        outVal = min(inVal,outMax) # clip at maximum value
    elif 'none':
        outVal = inVal
    else:
        print('ERROR in prune: incorrect clipType!')
    return outVal


# linlin
# map linear to linear range
def linlin(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    outVal = ( (prune(inVal, inMin, inMax, clipType) - inMin) / (inMax - inMin) ) * (outMax - outMin) + outMin
    return float(outVal) # make sure the output is always float

# linlin01
# map linear to linear range between 0 and 1, including minmax clipping
# faster alternative to linlin(inVal, inMin, inMax, 0, 1, 'minmax')
def linlin01(inVal, inMin, inMax):
    outVal = min(max( (inVal - inMin) / (inMax - inMin), 0), 1)
    return float(outVal)

# linexp
# map linear to exponential range
def linexp(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    if outMin==0 or outMax==0:
        print('ERROR in linexp: ZERO not possible for exponential range!')
    else:
        outVal = math.pow(outMax/outMin, (inVal-inMin)/(inMax-inMin)) * outMin
        return float( prune(outVal, outMin, outMax, clipType) )

# explin
# map exponential to linear range
def explin(inVal, inMin, inMax, outMin, outMax, clipType='minmax'):
    outVal = ( (math.log(prune(inVal, inMin, inMax, clipType)/inMin)) / (math.log(inMax/inMin)) ) * (outMax-outMin) + outMin
    return float(outVal)


# PARAMETERS
# setup debug printing
printPeriod = 0.05 # printing period


# MAIN LOOP
while True:

    lightRaw = light.value # read light value
    print(f'light (raw): {lightRaw}') # print raw light value

    lightLinlin = linlin(light.value, 900, 60000, 0, 1) # map light value
    print(f'light (linlin): {lightLinlin}') # print mapped value

    lightLinlin01 = linlin01(light.value, 900, 60000) # map light value
    print(f'light (linlin01): {lightLinlin01}') # print mapped value

    lightLinexp = linexp(light.value, 900, 60000, 0.01, 1) # map light value
    print(f'light (linexp): {lightLinexp}') # print mapped value

    lightExplin = explin(light.value, 900, 60000, 0, 1) # map light value
    print(f'light (explin): {lightExplin}') # print mapped value

    time.sleep(printPeriod) # sleep for 50ms
