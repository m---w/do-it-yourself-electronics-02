# DIY Electronics 02
# Assignment 6
# Example solution to get raw data from MPU-9250
# 2022 Marian Weger


# REQUIREMENTS
# (put in libs folder)
# - mpu9250.py from https://github.com/matemaciek/mpu9250_CircuitPython
# - adafruit_register folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - adafruit_bus_device folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/



# LIBRARIES
import board  # for the microcontroller itself
import mpu9250  # for MPU-9250
import time  #  for measuring time
import math  # basic math
from ulab import numpy as np  # linear algebra (vector math)



# SETUP
i2c = board.I2C()  # set up I2C connection (using default SCL and SCA pins)
imu = mpu9250.IMU(i2c)  # set up MPU-9250 IMU sensor via I2C


# Scheduling
printPeriod = 0.5  # print every 100ms
sensePeriod = 0.02  # read sensor values every 20ms

lastPrintTime = -1  # init variable
lastSenseTime = -1  # init variable

# init variables
magRaw = np.zeros(3)  # init raw magnetometer values
acc = np.zeros(3)  # init accelerometer values
gyr = np.zeros(3)  # init gyroscope values


# MAIN LOOP
while True:

    now = time.monotonic()  # what time is it now?

    # SENSOR PROCESSING
    if now >= lastSenseTime + sensePeriod:
        lastSenseTime = now

        # sensor reading
        acc[:] = imu.acc  # Accelerometer
        gyr[:] = imu.gyr  # Gyroscope
        magRaw[:] = imu.mag  # raw magnetometer values
        tmp = imu.tmp  # Thermometer


    # PRINTING
    if now >= lastPrintTime + printPeriod:
        lastPrintTime = now

        print("")  # blank line

        # print raw sensor data
        print("           x     y     z")
        print("Accel: {:5.2f} {:5.2f} {:5.2f}  g = 9.81 m/s^2".format(*acc))  # acceration in g = 9.80665 m/s^2
        print("Gyro:  {:5.0f} {:5.0f} {:5.0f}  deg/s".format(*gyr))  # rotation speed in degrees per second
        print("Mag:   {:5.0f} {:5.0f} {:5.0f}  uT = 10^-6 T".format(*magRaw))  # magnetic field in microtesla
        print("Temp:     {:.0f} deg C".format(tmp))  # in degrees Celsius

