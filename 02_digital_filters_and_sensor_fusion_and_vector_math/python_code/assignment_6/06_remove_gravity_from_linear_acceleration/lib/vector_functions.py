# 2022 Marian Weger
# for DIY Electronics 02

import math  # basic math
from ulab import numpy as np  # linear algebra (vector math)


def quat2rotMat(q):
	# 3x3 rotation matrix
	
    c00 = q[0] ** 2 + q[1] ** 2 - q[2] ** 2 - q[3] ** 2
    c01 = 2 * (q[1] * q[2] - q[0] * q[3])
    c02 = 2 * (q[1] * q[3] + q[0] * q[2])
    c10 = 2 * (q[1] * q[2] + q[0] * q[3])
    c11 = q[0] ** 2 - q[1] ** 2 + q[2] ** 2 - q[3] ** 2
    c12 = 2 * (q[2] * q[3] - q[0] * q[1])
    c20 = 2 * (q[1] * q[3] - q[0] * q[2])
    c21 = 2 * (q[2] * q[3] + q[0] * q[1])
    c22 = q[0] ** 2 - q[1] ** 2 - q[2] ** 2 + q[3] ** 2

    rotMat = np.array([[c00, c01, c02], [c10, c11, c12], [c20, c21, c22]])
    return rotMat


def rotMat2Euler(m):
    test = -m[2, 0]
    if test > 0.99999:
        yaw = 0
        pitch = np.pi / 2
        roll = np.arctan2(m[0, 1], m[0, 2])
    elif test < -0.99999:
        yaw = 0
        pitch = -np.pi / 2
        roll = np.arctan2(-m[0, 1], -m[0, 2])
    else:
        yaw = np.arctan2(m[1, 0], m[0, 0])
        pitch = np.asin(-m[2, 0])
        roll = np.arctan2(m[2, 1], m[2, 2])

    euler = np.zeros([3])  # init (yaw, pitch, roll)
    euler[0] = yaw
    euler[1] = pitch
    euler[2] = roll

	# convert radians to degrees
    euler = np.degrees( euler )

    return euler


def quat2Euler(q):
    m = quat2rotMat(q)
    return rotMat2Euler(m)


# https://newbedev.com/python-euler-to-rotation-matrix-python-code-example
def yaw2rotMat(yaw):
    yaw = np.radians(yaw)
    Rz_yaw = np.array([
        [np.cos(yaw), -np.sin(yaw), 0],
        [np.sin(yaw),  np.cos(yaw), 0],
        [          0,            0, 1]])
    return Rz_yaw


def pitch2rotMat(pitch):
    pitch = np.radians(pitch)
    Ry_pitch = np.array([
        [ np.cos(pitch), 0, np.sin(pitch)],
        [             0, 1,             0],
        [-np.sin(pitch), 0, np.cos(pitch)]])
    return Ry_pitch


def roll2rotMat(roll):
    roll = np.radians(roll)
    Rx_roll = np.array([
        [1,            0,             0],
        [0, np.cos(roll), -np.sin(roll)],
        [0, np.sin(roll),  np.cos(roll)]])
    return Rx_roll


# https://newbedev.com/python-euler-to-rotation-matrix-python-code-example
def euler2rotMat(yaw, pitch, roll):
    Rz_yaw = yaw2rotMat(yaw)
    Ry_pitch = pitch2rotMat(pitch)
    Rx_roll = roll2rotMat(roll)
    # R = RzRyRx
    rotMat = np.dot(Rz_yaw, np.dot(Ry_pitch, Rx_roll))
    return rotMat


def sphe2cart(r, el, az):
    theta = np.radians(el)
    phi = np.radians(az)

    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)

    xyz = np.zeros([3])  # init
    xyz[0] = x
    xyz[1] = y
    xyz[2] = z

    return xyz


def cart2sphe(xyz):
    xy = xyz[0]**2 + xyz[1]**2
    r = np.sqrt(xy + xyz[2]**2)
    # el = np.arctan2(np.sqrt(xy), xyz[:,2]) # for elevation angle defined from Z-axis down
    el = np.arctan2(xyz[2], np.sqrt(xy)) # for elevation angle defined from XY-plane up
    az = np.arctan2(xyz[1], xyz[0])

    sph = np.zeros([3])  # init
    sph[0] = r
    sph[1] = np.degrees(el)
    sph[2] = np.degrees(az)

    return sph
