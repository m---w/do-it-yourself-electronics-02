# DIY Electronics 02
# Assignment 6 example solution
# Set zero direction via button, and rotate a vector, get Azimuth and Elevation
# 2022 Marian Weger


# REQUIREMENTS
# (put in libs folder)
# - circuitpython_csv.mpy from https://github.com/tekktrik/CircuitPython_CSV/releases
# - mpu9250.py from https://github.com/matemaciek/mpu9250_CircuitPython
# - adafruit_register folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - adafruit_bus_device folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - adafruit_ticks.mpy from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - fusion: deltat.py, fusion.py, fusion_async.py, orientate.py from https://github.com/m---w/circuitpython-fusion


# LIBRARIES
import board  # for the microcontroller itself
import mpu9250  # for MPU-9250
import time  #  for measuring time
import math  # basic math
from ulab import numpy as np  # linear algebra (vector math)
import circuitpython_csv as csv  # to read csv files
import adafruit_ticks  # required by fusion library
from fusion import Fusion  # import sensor fusion library
import digitalio  # for digital ins/outs
import vector_functions as vf  # some custom conversion functions we will need



# SETUP
i2c = board.I2C()  # set up I2C connection (using default SCL and SCA pins)
imu = mpu9250.IMU(i2c)  # set up MPU-9250 IMU sensor via I2C
fuse = Fusion()  # set up sensor fusion
button = digitalio.DigitalInOut(board.D5)  # use D5 for button (patch button between D5 and ground)
button.direction = digitalio.Direction.INPUT  # set direction to input
button.pull = digitalio.Pull.UP  # activate pull-up resistor



# Scheduling
lastPrintTime = -1
printPeriod = 0.5
lastSenseTime = -1
sensePeriod = 0.02



#  LOAD CALIBRATION

# https://learn.adafruit.com/circuitpython-essentials/circuitpython-storage
# https://www.w3schools.com/python/python_file_handling.asp
# https://docs.python.org/3/library/csv.html
# https://stackoverflow.com/questions/17262256/how-to-read-one-single-line-of-csv-data-in-python
try:
    with open("/mag_offset.txt", "r") as f:
        csvreader = csv.reader(f, delimiter=' ')
        row = next(csvreader)  # read next line (i.e., the 1st one)
        magOffset = np.array([float(value) for value in row])
        print("Found mag offset:", magOffset)
        # f.close()  # close file (not needed, as "with" already takes care of it)
        
except OSError as e:  # File not found...
    print("No mag offset found. Using [0, 0, 0].")
    magOffset = np.zeros(3)

try:
    with open("/mag_scale.txt", "r") as f:
        csvreader = csv.reader(f, delimiter=' ')
        row = next(csvreader)  # read next line (i.e., the 1st one)
        magScale = np.array([float(value) for value in row])
        print("Found mag scale:", magScale)
        # f.close()  # close file (not needed, as "with" already takes care of it)
        
except OSError as e:  # File not found...
    print("No mag scale found. Using [0, 0, 0].")
    magScale = np.zeros(3)
    

# init variables
magRaw = np.zeros(3)  # init raw magnetometer values
acc = np.zeros(3)  # init accelerometer values
gyr = np.zeros(3)  # init gyroscope values
lastButtonState = False
diag = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])  # init identity matrix (no effect; ones in main diagonal, others zero)
rotMatZero = diag  # init rotation matrix as identity matrix
yawZero = 0.0  # init zero yaw direction as 0 (= north)
vec = np.array([1,0,0])  # define original vector (= Cartesian coordinate of a point in 3D we want to rotate)
    
# MAIN LOOP
while True:

    now = time.monotonic()  # what time is it now?
    
    # BUTTON PROCESSING
    buttonState = not button.value  # get current button state
    if buttonState != lastButtonState:
        if buttonState:
            print('######### Button pressed! ###########')
            
            yawZero = fuse.heading  # make snapshot of current heading
            rotMatZero = vf.yaw2rotMat(-yawZero)  # construct rotation matrix with negative yaw vector
            
            # Note: Instead of a button, you can also also simply add a hard-coded offset azimuth angle

            
    lastButtonState = buttonState  # in the end, update last button value

    # SENSOR PROCESSING
    if now >= lastSenseTime + sensePeriod:
        lastSenseTime = now

        # sensor reading
        acc[:] = imu.acc  # Accelerometer
        gyr[:] = imu.gyr  # Gyroscope
        magRaw[:] = imu.mag  # raw magnetometer values
        # tmp = imu.tmp  # Thermometer

        # apply calibration
        mag = (magRaw + magOffset) * magScale
        
        # sensor fusion
        fuse.update(acc, gyr, mag)  # update sensor fusion
        
        # Angle conversions
        rotMat = vf.quat2rotMat(fuse.q)  # get rotation matrix from quaternion
        
        # Zeroing
        rotMatZeroed = np.dot(rotMatZero, rotMat)  # combine zero matrix and rotation matrix, to include zeroed direction
        eulerZeroed = vf.rotMat2Euler(rotMatZeroed)  # get zeroed Euler angles
        
        # get azimuth and elevation (for spatialization)
        cartesian = np.dot(rotMatZeroed, vec)  # transform vector by final matrix
        spherical = vf.cart2sphe(cartesian)  # convert Cartesian to spherical coordinates
        
        # NOTE:
        # Easier way for zeroing: make snapshot of azimuth already in spherical coordinates, and then subtract the snapshot to get zeroed azimuth.
        # Zeroing the azimuth is necessary, e.g., for setting the direction of the stage, loudspeaker array, etc.
        # We usually don't need to zero the elevation if we are not on a boat/airplane/etc.
        
        # Make sure that elevation and azimuth is wrapped in correct range
        spherical[1] = math.fmod(spherical[1]+90, 180) - 90  # wrap elevation between -90 and +90 degrees
        spherical[2] = math.fmod(spherical[2]+180, 360) - 180  # wrap azimouth between -180 and +180 degrees
        
        
    if now >= lastPrintTime + printPeriod:
        lastPrintTime = now
        
        print("")  # blank line
        
        # print orientation as Tait-Bryan angles
        print("ORIENTATION yaw pitch roll")
        print("original:  {:4.0f}  {:4.0f} {:4.0f}  deg".format(fuse.heading, fuse.pitch, fuse.roll))
        print("zeroed:    {:4.0f}  {:4.0f} {:4.0f}  deg".format(*eulerZeroed))
        print('Rotated Vector:', *np.around(cartesian, decimals=2))
        print('--> Elevation:', round(spherical[1]), '/ Azimuth:', round(spherical[2]))
        

        

    

