# DIY Electronics 02
# Assignment 6 example solution
# Sensor Fusion
# 2022 Marian Weger


# REQUIREMENTS
# (put in libs folder)
# - circuitpython_csv.mpy from https://github.com/tekktrik/CircuitPython_CSV/releases
# - mpu9250.py from https://github.com/matemaciek/mpu9250_CircuitPython
# - adafruit_register folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - adafruit_bus_device folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - adafruit_ticks.mpy from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - fusion: deltat.py, fusion.py, fusion_async.py, orientate.py from https://github.com/m---w/circuitpython-fusion


# LIBRARIES
import board  # for the microcontroller itself
import mpu9250  # for MPU-9250
import time  #  for measuring time
import math  # basic math
from ulab import numpy as np  # linear algebra (vector math)
import circuitpython_csv as csv  # to read csv files
import adafruit_ticks  # required by fusion library
from fusion import Fusion  # import sensor fusion library



# SETUP
i2c = board.I2C()  # set up I2C connection (using default SCL and SCA pins)
imu = mpu9250.IMU(i2c)  # set up MPU-9250 IMU sensor via I2C
fuse = Fusion()  # set up sensor fusion


# Scheduling
lastPrintTime = -1
printPeriod = 0.5
lastSenseTime = -1
sensePeriod = 0.02



#  LOAD CALIBRATION

# https://learn.adafruit.com/circuitpython-essentials/circuitpython-storage
# https://www.w3schools.com/python/python_file_handling.asp
# https://docs.python.org/3/library/csv.html
# https://stackoverflow.com/questions/17262256/how-to-read-one-single-line-of-csv-data-in-python
try:
    with open("/mag_offset.txt", "r") as f:
        csvreader = csv.reader(f, delimiter=' ')
        row = next(csvreader)  # read next line (i.e., the 1st one)
        magOffset = np.array([float(value) for value in row])
        print("Found mag offset:", magOffset)
        # f.close()  # close file (not needed, as "with" already takes care of it)
        
except OSError as e:  # File not found...
    print("No mag offset found. Using [0, 0, 0].")
    magOffset = np.zeros(3)

try:
    with open("/mag_scale.txt", "r") as f:
        csvreader = csv.reader(f, delimiter=' ')
        row = next(csvreader)  # read next line (i.e., the 1st one)
        magScale = np.array([float(value) for value in row])
        print("Found mag scale:", magScale)
        # f.close()  # close file (not needed, as "with" already takes care of it)
        
except OSError as e:  # File not found...
    print("No mag scale found. Using [0, 0, 0].")
    magScale = np.zeros(3)
    

# init variables
magRaw = np.zeros(3)  # init raw magnetometer values
acc = np.zeros(3)  # init accelerometer values
gyr = np.zeros(3)  # init gyroscope values

    
# MAIN LOOP
while True:

    now = time.monotonic()  # what time is it now?

    # SENSOR PROCESSING
    if now >= lastSenseTime + sensePeriod:
        lastSenseTime = now

        # sensor reading
        acc[:] = imu.acc  # Accelerometer
        gyr[:] = imu.gyr  # Gyroscope
        magRaw[:] = imu.mag  # raw magnetometer values
        tmp = imu.tmp  # Thermometer

        # apply calibration
        mag = (magRaw + magOffset) * magScale
        
        # sensor fusion
        fuse.update(acc, gyr, mag)  # update sensor fusion
        
        
    # PRINTING
    if now >= lastPrintTime + printPeriod:
        lastPrintTime = now
        
        print("")  # blank line

        # print orientation as Tait-Bryan angles
        print("         yaw pitch  roll")
        print("Orient:{:5.0f} {:5.0f} {:5.0f}  deg".format(fuse.heading, fuse.pitch, fuse.roll))

        # print orientation as quaternion
        # print("           w     x     y     z")
        # print("Quat:  {:5.2f} {:5.2f} {:5.2f} {:5.2f}".format(*fuse.q))
        

        
    

    

