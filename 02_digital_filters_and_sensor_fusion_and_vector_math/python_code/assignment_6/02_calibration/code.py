# DIY Electronics 02
# Assignment 6 example solition
# Get magnetometer calibration via min/max of raw readings while rotating 
# 2022 Marian Weger


# REQUIREMENTS
# (put in libs folder)
# - mpu9250.py from https://github.com/matemaciek/mpu9250_CircuitPython
# - adafruit_register folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/
# - adafruit_bus_device folder from Adafruit CircuitPython Bundle: https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/



# LIBRARIES
import board  # for the microcontroller itself
import mpu9250  # for MPU-9250
import time  #  for measuring time
import math  # basic math
from ulab import numpy as np  # linear algebra (vector math)



# SETUP
i2c = board.I2C()  # set up I2C connection (using default SCL and SCA pins)
imu = mpu9250.IMU(i2c)  # set up MPU-9250 IMU sensor via I2C


# Scheduling
lastPrintTime = -1
printPeriod = 0.5
lastSenseTime = -1
sensePeriod = 0.02


# init variables
magRaw = np.zeros(3)  # init raw magnetometer values
# acc = np.zeros(3)  # init accelerometer values
# gyr = np.zeros(3)  # init gyroscope values
n = 0  # number of samples collected (since starting the program)
magMin = np.ndarray([999,999,999])  # init minimum as extremely high value
magMax = np.ndarray([-999,-999,-999])  # init maximum as extremely low value

# MAIN LOOP
while True:

    now = time.monotonic()  # what time is it now?

    # SENSOR PROCESSING
    if now >= lastSenseTime + sensePeriod:
        lastSenseTime = now

        # sensor reading
        # acc[:] = imu.acc  # Accelerometer
        # gyr[:] = imu.gyr  # Gyroscope
        magRaw[:] = imu.mag  # raw magnetometer values
        # tmp = imu.tmp  # Thermometer

        # get running min/max
        magMin = np.minimum(magRaw, magMin)  # take the minimum of both arrays
        magMax = np.maximum(magRaw, magMax)  # take the maximum of both arrays
        
        # hard iron offset
        magMean = np.mean(np.array([magMin, magMax]), axis=0)  # take mean: hard iron bias
        magOffset = -magMean  # correction offset (to be added to the raw magnetometer)
        
        # soft iron scale
        magRange = magMax - magMin  # get range
        magRangeMean = np.mean(magRange)  # get average range
        magFactor = magRange / magRangeMean  # get relative soft iron scale
        magScale = 1 / magFactor  # correction (to be multiplied with the raw magnetometer)
                
        n += 1  # increment number of samples
        

    # PRINTING
    if now >= lastPrintTime + printPeriod:
        lastPrintTime = now

        print("")  # blank line
        
        # print raw sensor data
        print("               x      y      z")
        print("Raw:         {:5.1f}  {:5.1f}  {:5.1f}  uT".format(*magRaw))  # magnetic field in microtesla
        print("Min:         {:5.1f}  {:5.1f}  {:5.1f}  uT".format(*magMin))  # min magnetic field in microtesla
        print("Max:         {:5.1f}  {:5.1f}  {:5.1f}  uT".format(*magMax))  # max magnetic field in microtesla
        print("Mean:        {:5.1f}  {:5.1f}  {:5.1f}  uT".format(*magMean))  # average of min and max
        print("Range (abs): {:5.1f}  {:5.1f}  {:5.1f}  uT".format(*magRange))  # range (between min and max)
        print("Range (rel):   {:1.3f}  {:1.3f}  {:1.3f}".format(*magFactor))  # range (between min and max)
        print("Calibration Values:")
        print("Offset: {:.8f} {:.8f} {:.8f}".format(*magOffset))  # hard iron offset
        print("Scale: {:.8f} {:.8f} {:.8f}".format(*magScale))  # soft iron scaling
    
        print("Number of recorded samples:", n)

        



