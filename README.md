# Do-It-Yourself Electronics 02


2022 Marian Weger

Course materials for a one semester course on digital DIY electronics held in 2022 at the University of Music and Performing Arts Graz, Austria.

## Objectives / Learning Outcomes

Building on "DIY Electronics 1",  students are familiar with elementary applications in the field of "Physical Computing". The existing knowledge in "Electronic Hacking" and "Circuit Bending" is extended by digital microprocessors. The students have the basic knowledge of digital controls based on microprocessors in theory and practice. They are able to design their own electronic devices using sensors and actuators - from MIDI controllers, to controlling sound generators, to autonomous instruments and automated sound installations.

## Content

Experiments are conducted with digital controls using up-to-date microcontrollers. Here you will use different sensors (knobs, switches, touchpads, accelerometers, etc.) to control actuators (motors, servos, solenoids, etc.) or analog electric circuits. Finally, own electronic devices will be created, ranging from MIDI controllers and the control of sound generators to autonomous instruments and automated sound installations. As a didactic approach, in the age of "Maker" culture, "DIY: Do it Yourself!" serves as a hands-on workshop for making your own sound generators, in the sense of DIY, as opposed to "Do It Like Others".
