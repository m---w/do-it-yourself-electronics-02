# MX1508 DC Motor Driver with PWM Control
# similar as L294D

# NOTE: only one direction works. Very strange.


import time
import board
import pwmio
from adafruit_motor import motor

INT1 = board.D9
INT2 = board.D10
INT3 = board.D11
INT4 = board.D12

# DC motor setup
pwmA1 = pwmio.PWMOut(INT1, frequency=50, duty_cycle=0)
pwmA2 = pwmio.PWMOut(INT2, frequency=50, duty_cycle=0)
motorA = motor.DCMotor(pwmA1, pwmA2)
pwmB1 = pwmio.PWMOut(INT3, frequency=50, duty_cycle=0)
pwmB2 = pwmio.PWMOut(INT4, frequency=50, duty_cycle=0)
motorB = motor.DCMotor(pwmB1, pwmB2)

print("***DC motor A test***")

print("\nForwards slow")
motorA.throttle = 0.5
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorA.throttle = None
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nForwards")
motorA.throttle = 1.0
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorA.throttle = None
print("throttle:", motorA.throttle)
time.sleep(1)

print("\nBackwards")
motorA.throttle = -1.0
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorA.throttle = None
print("throttle:", motorA.throttle)
time.sleep(1)

print("\nBackwards slow")
motorA.throttle = -0.5
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorA.throttle = None
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nSpin freely")
motorA.throttle = None
print("  throttle:", motorA.throttle)

print("\n***Motor A test is complete***")


print("***DC motor B test***")

print("\nForwards slow")
motorB.throttle = 0.5
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorB.throttle = None
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nForwards")
motorB.throttle = 1.0
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorB.throttle = None
print("throttle:", motorA.throttle)
time.sleep(1)

print("\nBackwards")
motorB.throttle = -1.0
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorB.throttle = None
print("throttle:", motorA.throttle)
time.sleep(1)

print("\nBackwards slow")
motorB.throttle = -0.5
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nStop")
motorB.throttle = None
print("  throttle:", motorA.throttle)
time.sleep(1)

print("\nSpin freely")
motorB.throttle = None
print("  throttle:", motorA.throttle)

print("\n***Motor B test is complete***")