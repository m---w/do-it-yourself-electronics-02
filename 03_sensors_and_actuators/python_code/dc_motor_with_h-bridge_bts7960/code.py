# DIY Electronics 02
# H-Bridge: BTS7960 with DC motor
# 2022 Marian Weger

# based on
# https://github.com/adafruit/Adafruit_CircuitPython_Motor/blob/main/examples/motor_h-bridge_dc_motor.py



# LIBRARIES
import board  # for the microcontroller itself
import time  #  for measuring time
import math  # basic math
import pwmio
import digitalio
from adafruit_motor import motor


# DC motor setup
# DC Motors generate electrical noise when running that can reset the microcontroller in extreme
# cases. A capacitor can be used to help prevent this.

# setup enable pins
L_EN = digitalio.DigitalInOut(board.D11)
R_EN = digitalio.DigitalInOut(board.D12)
L_EN.direction = digitalio.Direction.OUTPUT
R_EN.direction = digitalio.Direction.OUTPUT
L_EN.value = True  # set enabled by default
R_EN.value = True  # set enabled by default

# setup pwm pins
pwm_a = pwmio.PWMOut(board.D9, frequency=50)
pwm_b = pwmio.PWMOut(board.D10, frequency=50)
motor1 = motor.DCMotor(pwm_a, pwm_b)


while True:

	print("***DC motor test***")

	print("\nForwards slow")
	motor1.throttle = 0.5
	print("  throttle:", motor1.throttle)
	time.sleep(1)

	print("\nStop")
	motor1.throttle = 0
	print("  throttle:", motor1.throttle)
	time.sleep(1)

	print("\nForwards")
	motor1.throttle = 1.0
	print("  throttle:", motor1.throttle)
	time.sleep(1)

	print("\nStop")
	motor1.throttle = 0
	print("throttle:", motor1.throttle)
	time.sleep(1)

	print("\nBackwards")
	motor1.throttle = -1.0
	print("  throttle:", motor1.throttle)
	time.sleep(1)

	print("\nStop")
	motor1.throttle = 0
	print("throttle:", motor1.throttle)
	time.sleep(1)

	print("\nBackwards slow")
	motor1.throttle = -0.5
	print("  throttle:", motor1.throttle)
	time.sleep(1)

	print("\nStop")
	motor1.throttle = 0
	print("  throttle:", motor1.throttle)
	time.sleep(1)

	print("\nSpin freely")
	motor1.throttle = None
	print("  throttle:", motor1.throttle)

	print("\n***Motor test is complete***")



