# DIY Electronics 02
# Audio Out: play sine wave
# based on https://learn.adafruit.com/circuitpython-essentials/circuitpython-audio-out
# 2022 Marian Weger


import time
import array
import math
import board
import digitalio
from audiocore import RawSample

# load audioio library, depending on board
try:
    from audioio import AudioOut
except ImportError:
    try:
        from audiopwmio import PWMAudioOut as AudioOut
    except ImportError:
        pass  # not always supported by every board!


# PARAMETERS
tone_volume = 0.1  # Increase this to increase the volume of the tone.
frequency = 400  # Set this to the Hz of the tone you want to generate.


# CREATE SINE WAVE

# sample rat is 8kHz.
# one period of a sine wave (T=1/f) is 8000/f samples long
length = 8000 // frequency  # "//" instead of "/" outputs integer in case of only integer operands

# create new array of type "H" (unsigned short int) (see https://docs.python.org/3/library/array.html)
sine_wave = array.array("H", [0] * length)

# loop through samples and fill with sine wave, in unsigned integer format, including volume control
for i in range(length):
    sine_wave[i] = int((1 + math.sin(math.pi * 2 * i / length)) * tone_volume * (2 ** 15 - 1))


# SETUP
audio = AudioOut(board.A0)  # set A0 pin as audio out
sine_wave_sample = RawSample(sine_wave)  # create sound sample from the array


# MAIN LOOP
while True:
    audio.play(sine_wave_sample, loop=True)  # play sample in loop
    time.sleep(0.5)  # wait 0.5s
    audio.stop()  # stop sound playback
    time.sleep(0.5)  # wait 0.5s
