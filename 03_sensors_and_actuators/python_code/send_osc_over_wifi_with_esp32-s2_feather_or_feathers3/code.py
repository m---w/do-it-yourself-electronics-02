import ipaddress
import wifi
import socketpool
import time
import random
from uosc import client as osc  # https://github.com/SpotlightKid/micropython-osc/tree/master/uosc

HOST_IP = "192.168.1.8"
HOST_PORT = 6000
HOST = (HOST_IP, HOST_PORT)

# Get wifi details and more from a secrets.py file
try:
    from secrets import secrets
except ImportError:
    print("WiFi secrets are kept in secrets.py, please add them there!")
    print("    --- File contents of secrets.py: --")
    print("    secrets = {'ssid' : 'your-ssid-here', 'password' : 'your-password-here'}")
    print("    --- end ---")
    raise


# Print MAC address
print("My MAC addr:", [hex(i) for i in wifi.radio.mac_address])

# Scan and print available networks
print("Available WiFi networks:")
for network in wifi.radio.start_scanning_networks():
    print("\t%s\t\tRSSI: %d\tChannel: %d" % (str(network.ssid, "utf-8"),
            network.rssi, network.channel))
wifi.radio.stop_scanning_networks()

# Pack connection into function for later reconnect
def connect(secrets):
    
    # Connect to network
    print("Connecting to %s"%secrets["ssid"])
    wifi.radio.connect(secrets["ssid"], secrets["password"])
    print("Connected to %s!"%secrets["ssid"])
    print("My IP address is", wifi.radio.ipv4_address)
    
    # Create Socket
    pool = socketpool.SocketPool(wifi.radio)  # only one socketpool per wifi can be created
    s = pool.socket(pool.AF_INET, pool.SOCK_DGRAM)  # create the socket. SOCK_DGRAM for UDP.
    return pool, s
    
    
# Connect
pool, s = connect(secrets)

# Ping OSC Server
ipv4 = ipaddress.ip_address(HOST_IP)
print("Ping OSC server: %f ms" % (wifi.radio.ping(ipv4)*1000))


print("sending continuously...")
while True:
    try:
        # try to send message
        oscMessage = osc.create_message('/foo/bar', random.random())  # CREATE OSC MESSAGE. message is constructed from address
        s.sendto(oscMessage, HOST)  # just send to the host address. No connection needed for UDP client.
        time.sleep(0.1)
    except:
        # otherwise reconnect to network
        s.close()  # close socket
        wifi.radio.enabled = False  #  disable wifi
        wifi.radio.enabled = True  #  enable again
        pool, s = connect(secrets)
