# This file is where you keep secret settings, passwords, and tokens!
# If you put them in the code you risk committing that info or sharing it

secrets = {
    'ssid' : 'YOUR_WIFI_NAME',
    'password' : 'YOUR_WIFI_PASSWORD',
    'timezone' : "Europe/Vienna", # http://worldtimeapi.org/timezones
    }

