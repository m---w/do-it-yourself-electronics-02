# DIY Electronics 02
# 2022, Marian Weger
# Piezo Output PWM example
# adapted from https://learn.adafruit.com/circuitpython-essentials/circuitpython-pwm

# Circuit: Connect piezo between A1 and GND.
# If using raw piezo disk, attach it to a resonator.

import time
import board
import simpleio
import random

# Settings
scale = [262, 294, 330, 349, 392, 440, 494, 523]  # c major scale
bpm = 180  # 180 beats per minute
noteLen = 0.85  # relative length of a note

# Pre-computations
spb = 60/bpm  # seconds per beat
pauseLen = 1 - noteLen  # relative length of  pauses between
noteDur = noteLen * spb  # absolute note duration
pauseDur = pauseLen * spb  # absolute pause duration


while True:
    for n in range(7):
        freq = random.choice(scale)  # choose note
        simpleio.tone(board.A1, freq, noteDur)  # play note
        time.sleep(pauseDur)  # pause
    time.sleep(spb)  # pause one period
    print('Enjoy the wonderful music!')
