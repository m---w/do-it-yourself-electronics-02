# DIY Electronics 02
# Servo motors with PCA9685 16-channel PWM & Servo Driver
# 2022 Marian Weger

# based on adafruit example

# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

"""Simple test for a standard servo on channel 0 and a continuous rotation servo on channel 1."""
import time
from adafruit_servokit import ServoKit

# Set channels to the number of servo channels on your kit.
# 8 for FeatherWing, 16 for Shield/HAT/Bonnet.
kit = ServoKit(channels=16)


kit.servo[0].angle = 180
print("180 deg, throttle +1")
kit.continuous_servo[1].throttle = 1
time.sleep(1)
print("180 deg, throttle -1")
kit.continuous_servo[1].throttle = -1
time.sleep(1)
kit.servo[0].angle = 0
print("0 deg, throttle 1")
kit.continuous_servo[1].throttle = 0
time.sleep(1)
