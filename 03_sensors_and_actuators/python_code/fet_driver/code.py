# DIY Electronics 02
# Trigger solenoid via FET driver
# 2022 Marian Weger

# e.g., with IRF520 FET driver module

# LIBRARIES
import board # for the microcontroller itself
import time #  for measuring time
import math # basic math
import digitalio # for digital ins/outs

# SETUP INPUTS AND OUTPUTS
solenoid = digitalio.DigitalInOut(board.D10) # setup digital i/o pin 10
solenoid.direction = digitalio.Direction.OUTPUT # set pin as output

# INIT PARAMETERS
triggerPeriod = 1.5 # trigger period
triggerDur = 0.03 # active duration

# PRE-COMPUTATIONS
triggerSleep = triggerPeriod - triggerDur # inactive duration


# MAIN LOOP
while(True):

    solenoid.value = True # set digital output to High
    time.sleep(triggerDur) # wait for active duration (not too long!)
    solenoid.value = False # set digital output to Low
    time.sleep(triggerSleep) # wait for inactive duration