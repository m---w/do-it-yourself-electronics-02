# DIY Electronics 02
# H-Bridge: HG7881 with DC motor
# 2022 Marian Weger

# https://bananarobotics.com/shop/How-to-use-the-HG7881-(L9110)-Dual-Channel-Motor-Driver-Module



# LIBRARIES
import board  # for the microcontroller itself
import time  #  for measuring time
import math  # basic math
import pwmio
import digitalio

A_IA = board.D10
A_IB = board.D9
B_IA = board.D12
B_IB = board.D11

pwmA = pwmio.PWMOut(A_IA, duty_cycle = 0, frequency=50, variable_frequency=False)
pwmB = pwmio.PWMOut(B_IA, duty_cycle = 0, frequency=50, variable_frequency=False)

dirA = digitalio.DigitalInOut(A_IB)
dirB = digitalio.DigitalInOut(B_IB)

dirA.direction = digitalio.Direction.OUTPUT
dirB.direction = digitalio.Direction.OUTPUT

dirA.value = True
dirB.value = False


print("***DC motor A test***")

print("\nForwards slow")
dirA.value = True
pwmA.duty_cycle = 65535 // 2  # On 50%
time.sleep(1)

print("\nStop")
dirA.value = True
pwmA.duty_cycle = 65535  # Off
time.sleep(1)

print("\nBackwards slow")
dirA.value = False
pwmA.duty_cycle = 65535 // 2  # On 50%
time.sleep(1)

print("\nStop")
dirA.value = False
pwmA.duty_cycle = 0  # Off
time.sleep(1)

print("\nForwards fast")
dirA.value = True
pwmA.duty_cycle = 0  # On 50%
time.sleep(1)

print("\nStop")
dirA.value = True
pwmA.duty_cycle = 65535  # Off
time.sleep(1)

print("\nBackwards fast")
dirA.value = False
pwmA.duty_cycle = 65535  # On 50%
time.sleep(1)

print("\nStop")
dirA.value = False
pwmA.duty_cycle = 0  # Off
time.sleep(1)


print("\n***Motor test B is complete***")



print("***DC motor B test***")

print("\nForwards slow")
dirB.value = True
pwmB.duty_cycle = 65535 // 2  # On 50%
time.sleep(1)

print("\nStop")
dirB.value = True
pwmB.duty_cycle = 65535  # Off
time.sleep(1)

print("\nBackwards slow")
dirB.value = False
pwmB.duty_cycle = 65535 // 2  # On 50%
time.sleep(1)

print("\nStop")
dirB.value = False
pwmB.duty_cycle = 0  # Off
time.sleep(1)

print("\nForwards fast")
dirB.value = True
pwmB.duty_cycle = 0  # On 50%
time.sleep(1)

print("\nStop")
dirB.value = True
pwmB.duty_cycle = 65535  # Off
time.sleep(1)

print("\nBackwards fast")
dirB.value = False
pwmB.duty_cycle = 65535  # On 50%
time.sleep(1)

print("\nStop")
dirB.value = False
pwmB.duty_cycle = 0  # Off
time.sleep(1)


print("\n***Motor test B is complete***")