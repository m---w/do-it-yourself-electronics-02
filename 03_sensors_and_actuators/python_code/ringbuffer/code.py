# DIY Electronics 02
# Ring buffer Examples
# 2022 Marian Weger


import board
import math
import time
from ulab import numpy as np
import random


buflen = 4  # length of ringbuffer
ringbuf = np.zeros(buflen)  # ringbuffer, init with zeros
i = 0  # starting index


while True:

    # s = random.random()  # random float sensor value
    s = random.randint(0,9)  # random int sensor value between 0 and 9
    ringbuf[i] = s  # write value to ringbuffer
    i = (i + 1) % buflen  # increment index in ringbuffer
    i %= buflen  # wrap index at buffer length via modulo

    print('Ringbuffer:', list(ringbuf))  # print ringbuffer contents
    print('cursor:', i)  # print current cursor position

    med = np.median(ringbuf)  # Median filter
    print('median:', med)

    avg = np.mean(ringbuf)  # Moving average filter
    print('moving average:', avg)

    std = np.std(ringbuf)  # Moving standard deviation
    print('standard deviation:', round(std,2))
    
    time.sleep(1.0)  # sleep a second
