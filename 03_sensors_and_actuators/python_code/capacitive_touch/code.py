# DIY Electronics 02
# Capacitive Touch
# 2022 Marian Weger
#
# adapted from https://learn.adafruit.com/circuitpython-essentials/circuitpython-cap-touch

import board
import time
import touchio

touch1 = touchio.TouchIn(board.A1) # set up capacitive touch at pin A1
# Note for M4 and some other boards: 
# Works with all analog inputs, but requires 1MOhm pull-down resistor from input pin to ground.


lastTouchState1 = False

while True:

    touchState1 = touch1.value # read sensor value

    if touchState1:
        if touchState1 != lastTouchState1:
            print("Touch 1: On")
        lastTouchState1 = True
    else:
        if touchState1 != lastTouchState1:
            print("Touch 1: Off")
        lastTouchState1 = False

    time.sleep(0.05)