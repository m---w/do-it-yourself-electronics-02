# DIY Electronics 02
# Stepper Motor with ST-A4988 (max 35V / 2A) or DRV8825
# 2022 Marian Weger

# based on:
# https://projects-raspberry.com/raspberry-pi-pico-stepper-motor-examples/
# https://elfnor.com/micropython-stepper-motor-control-with-a-a4988-carrier-board.html
# https://www.makerguides.com/drv8825-stepper-motor-driver-arduino-tutorial/


# LIBRARIES
import board  # for the microcontroller itself
import time  #  for measuring time
import math  # basic math
import digitalio




#--------------------------------------------------------------------------------
class A4988:
    def __init__(self, DIR=board.D11, STEP=board.D10):
        """This class represents an A4988 stepper motor driver.  It uses two output pins
        for direction and step control signals."""

        self._dir  = digitalio.DigitalInOut(DIR)
        self._step = digitalio.DigitalInOut(STEP)

        self._dir.direction  = digitalio.Direction.OUTPUT
        self._step.direction = digitalio.Direction.OUTPUT

        self._dir.value = False
        self._step.value = False

    def step(self, forward=True):
        """Emit one step pulse, with an optional direction flag."""
        self._dir.value = forward

        # Create a short pulse on the step pin.  Note that CircuitPython is slow
        # enough that normal execution delay is sufficient without actually
        # sleeping.
        self._step.value = True
        # time.sleep(1e-6)
        self._step.value = False

    def move_sync(self, steps, speed=1000.0):
        """Move the stepper motor the signed number of steps forward or backward at the
        speed specified in steps per second.  N.B. this function will not return
        until the move is done, so it is not compatible with asynchronous event
        loops.
        """

        self._dir.value = (steps >= 0)
        time_per_step = 1.0 / speed
        for count in range(abs(steps)):
            self._step.value = True
            # time.sleep(1e-6)
            self._step.value = False
            time.sleep(time_per_step)

    def deinit(self):
        """Manage resource release as part of object lifecycle."""
        self._dir.deinit()
        self._step.deinit()
        self._dir  = None
        self._step = None

    def __enter__(self):
        return self

    def __exit__(self):
        # Automatically deinitializes the hardware when exiting a context.
        self.deinit()

#--------------------------------------------------------------------------------
# Stepper motor demonstration.

stepper = A4988()
print("Starting stepper motor test.")

speed = 100

# MOTORS

# 35BY412M-260
# 12V / 500mA / 48 steps/round (full step) / reaches until ~280 steps/second
# 7.5 deg/step

#
# full step
# 200 steps/round (1.8 deg/step)
# reaches until ~650 steps/second

# Stepper Controller
# DRV8825 and A4988 are pin and code compatible


while True:
    print(f"Speed: {speed} steps/sec.")
    stepper.move_sync(200, speed)
    time.sleep(1.0)

    stepper.move_sync(-200, speed)
    time.sleep(1.0)

    speed *= 1.2
    if speed > 500:
        speed = 100




